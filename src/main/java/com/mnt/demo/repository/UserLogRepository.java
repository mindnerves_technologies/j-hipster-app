package com.mnt.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mnt.demo.domain.User;
import com.mnt.demo.domain.UserLogs;

/**
 * Spring Data JPA repository for the {@link User} entity.
 */
@Repository
public interface UserLogRepository extends JpaRepository<UserLogs, Long> {
   
}
