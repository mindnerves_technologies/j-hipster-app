package com.mnt.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mnt.demo.domain.OrderList;
import com.mnt.demo.domain.User;
import com.mnt.demo.domain.UserLogs;

/**
 * Spring Data JPA repository for the {@link User} entity.
 */
@Repository
public interface OrderListRepository extends JpaRepository<OrderList, Long> {

}
