package com.mnt.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mnt.demo.domain.ScanData;
import com.mnt.demo.domain.User;

/**
 * Spring Data JPA repository for the {@link User} entity.
 */
@Repository
public interface ScanDataRepository extends JpaRepository<ScanData, Long> {
}
