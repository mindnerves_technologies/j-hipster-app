package com.mnt.demo.repository;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mnt.demo.domain.LoggedInUsers;
import com.mnt.demo.domain.User;

/**
 * Spring Data JPA repository for the {@link User} entity.
 */
@Repository
public interface UserLoggedInRepository extends JpaRepository<LoggedInUsers, Long> {

	@Transactional
	@Modifying
	@Query(value = "update logged_in_users set modified_date =:todaysDate where id=:id", nativeQuery = true)
	int updateLoggedInUser(@Param("todaysDate") Date todaysDate, @Param("id") long id);
   
}
