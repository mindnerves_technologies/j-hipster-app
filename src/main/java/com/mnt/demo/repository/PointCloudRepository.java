package com.mnt.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mnt.demo.domain.PointCloud;

@Repository
public interface PointCloudRepository extends JpaRepository<PointCloud, Long> {
	PointCloud findByScanName(String scanName);
}
