package com.mnt.demo.service.dto;

/**
 * A DTO representing a user, with his authorities.
 */
public class Login {

	
	private String email;
	private String password;
	private long id;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "Login [email=" + email + ", password=" + password + ", id=" + id + "]";
	}
	
	
}
