package com.mnt.demo.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import javax.validation.ValidationException;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bigcommerce.BigcommerceSdk;
import com.bigcommerce.catalog.models.CatalogSummary;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mnt.demo.domain.PointCloud;
import com.mnt.demo.repository.PointCloudRepository;
import com.mnt.demo.web.rest.vm.PointCloudVM;
import com.mnt.demo.web.rest.vm.RestResponse;

@Service
public class PointCloudService {

	@Autowired
	PointCloudRepository pointCloudRepository;
	Predicate<String> nullEmptyTest = n -> n == null || n.equals("");
	Predicate<List<?>> nullZeroTest = n -> n == null || n.size() == 0;

	public RestResponse savePoints(PointCloudVM pointCloudVM) {
		RestResponse res = new RestResponse();
		try {
			if (nullEmptyTest.test(pointCloudVM.getScanName())) {
				res.put("message", "Scan name is Empty.");
				res.put("status", "failure");
				res.put("code", "400");
				return res;
			} else if (nullZeroTest.test(pointCloudVM.getPoints())) {
				res.put("message", "Points are Empty.");
				res.put("status", "failure");
				res.put("code", "400");
				return res;
			}
			PointCloud pointCloud = null;
			pointCloud = pointCloudRepository.findByScanName(pointCloudVM.getScanName());
			if (Objects.nonNull(pointCloud)) {
				res.put("message", "Point Cloud Exist With Same Scan Name.");
				res.put("status", "failure");
				res.put("code", "500");
				return res;
			} else
				pointCloud = new PointCloud();
			pointCloud.setScanName(pointCloudVM.getScanName());
			pointCloud.setPoints(pointCloudVM.getPoints().toString());
			res.put("data", pointCloudRepository.save(pointCloud));
			res.put("status", "success");
			res.put("message", "Point Cloud Saved.");
			res.put("code", "200");
		} catch (ValidationException e) {
			res.put("message", "Scan name is Invalid.");
			res.put("status", "failure");
			res.put("code", "400");
		} catch (Exception e) {
			e.printStackTrace();
			res.put("message", "Point Cloud Not Saved.");
			res.put("status", "failure");
			res.put("code", "400");
		}
		return res;
	}

	public RestResponse saveBigCommerceResponse(RestResponse restResponse) {
		RestResponse res = new RestResponse();
//		JSONObject obj = new JSONObject();
//		obj.put("requestData", "obj");
//		try {
////			HttpResponse<String> response = Unirest.post("https://api.bigcommerce.com/stores")
////					.header("Content-Type", "application/json").body(obj).asString();
//			res.put("response", Unirest.post("https://api.bigcommerce.com/stores")
//					.header("Content-Type", "application/json").body(obj).asString());
//		} catch (UnirestException e) {
//			e.printStackTrace();
//		}
		
		final BigcommerceSdk bigcommerceSdk = BigcommerceSdk.newBuilder()
				.withStoreHash("lkd1kmbr4q")
				.withClientId("ss54be9yj3waxjz712c8yrd8dy19s3d").withAccessToken("sddq7nzn6j8eh2hn6dgx2t6sl91u3fx").build();
//		final CatalogSummary catalogSummary = bigcommerceSdk.getCatalogSummary();
		res.put("SOME_STORE_HASH", bigcommerceSdk.getStoreHash());
		res.put("SOME_CLIENT_ID", bigcommerceSdk.getClientId());
		res.put("SOME_ACCESS_TOKEN", bigcommerceSdk.getAccessToken());
//		res.put("Catalog Summary", bigcommerceSdk);
		res.put("data", restResponse.get("key"));
		return res;
	}
}
