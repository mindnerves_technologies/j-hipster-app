package com.mnt.demo.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "order_list")
@JsonIgnoreProperties(ignoreUnknown = true, value = {"hibernateLazyInitializer", "handler" })
public class OrderList extends AbstractAuditingEntity implements Serializable {
		private static final long serialVersionUID = 1L;	
	  	
		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

		@NotNull(message="Scan id cannot be empty")
		@ManyToOne(fetch = FetchType.LAZY)
	    @JoinColumn(name = "scan_id")
		private ScanData scan;

		@NotNull(message="Date cannot be empty")
		@Size(max = 50)
		@Column(name = "delivery_date")
		private Date deliveryDate;
		

		@NotNull(message="Status cannot be empty")
		@Size(max = 50)
		@Column(name = "status")
		private String status;
		
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public ScanData getScan() {
			return scan;
		}

		public void setScan(ScanData scan) {
			this.scan = scan;
		}

		public Date getDeliveryDate() {
			return deliveryDate;
		}

		public void setDeliveryDate(Date deliveryDate) {
			this.deliveryDate = deliveryDate;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}
		
		
}
