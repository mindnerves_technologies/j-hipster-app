package com.mnt.demo.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
   
@Entity
@Table(name = "logged_in_users")
public class LoggedInUsers extends AbstractAuditingEntity implements Serializable {
		private static final long serialVersionUID = 1L;	
	  	@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

	    @Column(name = "modified_date")
	    private Date modifiedDate;

	    @Column(name = "count")
	    private int count;

	    @Size(min = 60, max = 60)
	    @Column(name = "password_hash", length = 60, nullable = false)
	    private String password;

	    @Email
	    @Size(min = 5, max = 254)
	    @Column(length = 254, unique = true)
	    private String email;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Date getModifiedDate() {
			return modifiedDate;
		}

		public void setModifiedDate(Date modifiedDate) {
			this.modifiedDate = modifiedDate;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public int getCount() {
			return count;
		}

		public void setCount(int count) {
			this.count = count;
		}

		public static long getSerialversionuid() {
			return serialVersionUID;
		}
}
