package com.mnt.demo.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
   
@Entity
@Table(name = "user_logs")
public class UserLogs extends AbstractAuditingEntity implements Serializable {
		private static final long serialVersionUID = 1L;	
	  	@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

	    @Size(max = 50)
	    @Column(name = "authentication_status", length = 50)
	    private String authenticationStatus;

	    @Column(name = "modified_date")
	    private Date modifiedDate;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getAuthenticationStatus() {
			return authenticationStatus;
		}

		public void setAuthenticationStatus(String authenticationStatus) {
			this.authenticationStatus = authenticationStatus;
		}

		public Date getModifiedDate() {
			return modifiedDate;
		}

		public void setModifiedDate(Date modifiedDate) {
			this.modifiedDate = modifiedDate;
		}
		
		
}
