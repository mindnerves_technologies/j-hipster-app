package com.mnt.demo.domain;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Entity
public class PointCloud {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long scanId;
	@Pattern(regexp = "[a-zA-Z0-9]|*")
	private String scanName;
	@Column(columnDefinition = "TEXT", length = 260)
	private String points;

	public Long getScanId() {
		return scanId;
	}

	public void setScanId(Long scanId) {
		this.scanId = scanId;
	}

	public String getScanName() {
		return scanName;
	}

	public void setScanName(String scanName) {
		this.scanName = scanName;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}
}
