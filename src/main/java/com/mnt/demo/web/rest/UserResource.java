package com.mnt.demo.web.rest;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mnt.demo.config.Constants;
import com.mnt.demo.domain.LoggedInUsers;
import com.mnt.demo.domain.OrderList;
import com.mnt.demo.domain.ScanData;
import com.mnt.demo.domain.User;
import com.mnt.demo.domain.UserLogs;
import com.mnt.demo.repository.OrderListRepository;
import com.mnt.demo.repository.ScanDataRepository;
import com.mnt.demo.repository.UserLogRepository;
import com.mnt.demo.repository.UserLoggedInRepository;
import com.mnt.demo.repository.UserRepository;
import com.mnt.demo.security.AuthoritiesConstants;
import com.mnt.demo.service.MailService;
import com.mnt.demo.service.UserService;
import com.mnt.demo.service.dto.Login;
import com.mnt.demo.service.dto.UserDTO;
import com.mnt.demo.web.rest.errors.BadRequestAlertException;
import com.mnt.demo.web.rest.errors.EmailAlreadyUsedException;
import com.mnt.demo.web.rest.errors.LoginAlreadyUsedException;
import com.mnt.demo.web.rest.vm.RestResponse;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing users.
 * <p>
 * This class accesses the {@link User} entity, and needs to fetch its
 * collection of authorities.
 * <p>
 * For a normal use-case, it would be better to have an eager relationship
 * between User and Authority, and send everything to the client side: there
 * would be no View Model and DTO, a lot less code, and an outer-join which
 * would be good for performance.
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities,
 * because people will quite often do relationships with the user, and we don't
 * want them to get the authorities all the time for nothing (for performance
 * reasons). This is the #1 goal: we should not impact our users' application
 * because of this use-case.</li>
 * <li>Not having an outer join causes n+1 requests to the database. This is not
 * a real issue as we have by default a second-level cache. This means on the
 * first HTTP call we do the n+1 requests, but then all authorities come from
 * the cache, so in fact it's much better than doing an outer join (which will
 * get lots of data from the database, for each HTTP call).</li>
 * <li>As this manages users, for security reasons, we'd rather have a DTO
 * layer.</li>
 * </ul>
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this
 * case.
 */
@RestController
@RequestMapping("/api")
public class UserResource {
	
	@Value("${spring.servlet.location}")
	public String path;
	
	private static final List<String> ALLOWED_ORDERED_PROPERTIES = Collections
			.unmodifiableList(Arrays.asList("id", "login", "firstName", "lastName", "email", "activated", "langKey"));

	private final Logger log = LoggerFactory.getLogger(UserResource.class);

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final UserService userService;

	private final UserRepository userRepository;

	private final MailService mailService;

	private final UserLogRepository userLogRepository;

	private final UserLoggedInRepository userLoggedInRepository;
	
	private final OrderListRepository orderListRepository;

	private final ScanDataRepository scanDataRepository;
	public UserResource(UserService userService, UserRepository userRepository, MailService mailService,
			UserLogRepository userLogRepository, UserLoggedInRepository userLoggedInRepository, ScanDataRepository scanDataRepository,
			OrderListRepository orderListRepository) {
		this.userService = userService;
		this.userRepository = userRepository;
		this.mailService = mailService;
		this.userLogRepository = userLogRepository;
		this.userLoggedInRepository = userLoggedInRepository;
		this.scanDataRepository = scanDataRepository;
		this.orderListRepository = orderListRepository;
	}

	/**
	 * {@code POST  /users} : Creates a new user.
	 * <p>
	 * Creates a new user if the login and email are not already used, and sends an
	 * mail with an activation link. The user needs to be activated on creation.
	 *
	 * @param userDTO the user to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new user, or with status {@code 400 (Bad Request)} if the
	 *         login or email is already in use.
	 * @throws URISyntaxException       if the Location URI syntax is incorrect.
	 * @throws BadRequestAlertException {@code 400 (Bad Request)} if the login or
	 *                                  email is already in use.
	 */
	@PostMapping("/users")
	@PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
	public ResponseEntity<User> createUser(@Valid @RequestBody UserDTO userDTO) throws URISyntaxException {
		log.debug("REST request to save User : {}", userDTO);

		if (userDTO.getId() != null) {
			throw new BadRequestAlertException("A new user cannot already have an ID", "userManagement", "idexists");
			// Lowercase the user login before comparing with database
		} else if (userRepository.findOneByLogin(userDTO.getLogin().toLowerCase()).isPresent()) {
			throw new LoginAlreadyUsedException();
		} else if (userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).isPresent()) {
			throw new EmailAlreadyUsedException();
		} else {
			User newUser = userService.createUser(userDTO);
			mailService.sendCreationEmail(newUser);
			return ResponseEntity.created(new URI("/api/users/" + newUser.getLogin()))
					.headers(HeaderUtil.createAlert(applicationName, "userManagement.created", newUser.getLogin()))
					.body(newUser);
		}
	}

	/**
	 * {@code PUT /users} : Updates an existing User.
	 *
	 * @param userDTO the user to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated user.
	 * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is
	 *                                   already in use.
	 * @throws LoginAlreadyUsedException {@code 400 (Bad Request)} if the login is
	 *                                   already in use.
	 */
	@PutMapping("/users")
	@PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
	public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserDTO userDTO) {
		log.debug("REST request to update User : {}", userDTO);
		Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
		if (existingUser.isPresent() && (!existingUser.get().getId().equals(userDTO.getId()))) {
			throw new EmailAlreadyUsedException();
		}
		existingUser = userRepository.findOneByLogin(userDTO.getLogin().toLowerCase());
		if (existingUser.isPresent() && (!existingUser.get().getId().equals(userDTO.getId()))) {
			throw new LoginAlreadyUsedException();
		}
		Optional<UserDTO> updatedUser = userService.updateUser(userDTO);

		return ResponseUtil.wrapOrNotFound(updatedUser,
				HeaderUtil.createAlert(applicationName, "userManagement.updated", userDTO.getLogin()));
	}

	/**
	 * {@code GET /users} : get all users.
	 *
	 * @param pageable the pagination information.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         all users.
	 */
	@GetMapping("/users")
	public ResponseEntity<List<UserDTO>> getAllUsers(Pageable pageable) {
		if (!onlyContainsAllowedProperties(pageable)) {
			return ResponseEntity.badRequest().build();
		}

		final Page<UserDTO> page = userService.getAllManagedUsers(pageable);
		HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	private boolean onlyContainsAllowedProperties(Pageable pageable) {
		return pageable.getSort().stream().map(Sort.Order::getProperty).allMatch(ALLOWED_ORDERED_PROPERTIES::contains);
	}

	/**
	 * Gets a list of all roles.
	 * 
	 * @return a string list of all roles.
	 */
	@GetMapping("/users/authorities")
	@PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
	public List<String> getAuthorities() {
		return userService.getAuthorities();
	}

	/**
	 * {@code GET /users/:login} : get the "login" user.
	 *
	 * @param login the login of the user to find.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the "login" user, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
	public ResponseEntity<UserDTO> getUser(@PathVariable String login) {
		log.debug("REST request to get User : {}", login);
		return ResponseUtil.wrapOrNotFound(userService.getUserWithAuthoritiesByLogin(login).map(UserDTO::new));
	}

	/**
	 * {@code DELETE /users/:login} : delete the "login" User.
	 *
	 * @param login the login of the user to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
	@PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
	public ResponseEntity<Void> deleteUser(@PathVariable String login) {
		log.debug("REST request to delete User: {}", login);
		userService.deleteUser(login);
		return ResponseEntity.noContent()
				.headers(HeaderUtil.createAlert(applicationName, "userManagement.deleted", login)).build();
	}

	@PostMapping("/login")
	@ResponseBody
	public RestResponse login(@RequestBody Login vm, HttpServletRequest request) {
		RestResponse response = new RestResponse();
		UserLogs log = new UserLogs();
		LoggedInUsers users = new LoggedInUsers();
		Date todaysDate = new Date();
		int count = 0;
		try {
			if ((vm.getEmail() != null) && (vm.getPassword() != null)) {
				String userName = vm.getEmail();
				String password = vm.getPassword();
				User user = userRepository.findUserForLogin(userName, password);

				if (user != null) {
					if (userLoggedInRepository.findById(user.getId()) != null) {
						userLoggedInRepository.updateLoggedInUser(todaysDate, user.getId());
					} else {
						users.setModifiedDate(todaysDate);
						users.setEmail(vm.getEmail());
						users.setPassword(vm.getPassword());
						users.setCount(count);
						userLoggedInRepository.save(users);
						response.asData(users);
					}
					log.setAuthenticationStatus("Succuess");
					log.setModifiedDate(todaysDate);
					userLogRepository.save(log);
					response.asCode(200);
					response.asMessage("Success");
					return response;
				} else {
				
					log.setAuthenticationStatus("Failed");
					log.setModifiedDate(todaysDate);
					userLogRepository.save(log);
					response.asCode(400);
					response.asMessage("Failed");
					response.asMessage("Please check your login credientials");
					return response;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@PostMapping("/scan/data")
	@ResponseBody
	public RestResponse handleFileUpload(@RequestParam("file") MultipartFile multipartFile) {
		RestResponse response = new RestResponse();
		if (multipartFile.isEmpty()) {
			ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Request Must Contatin File");
		}
		boolean f = uploadFile(multipartFile);
		if (f) {
			ScanData scanData = new ScanData();
			scanData.setFileName(multipartFile.getOriginalFilename());
			scanData.setFileLocation(path);
			Date date = new Date();
			scanData.setUpdatedDate(date);
			scanData.setCreatedBy("System");
			scanDataRepository.save(scanData);
			response.asCode(200);
			response.asMessage("Success");
			response.asData(scanData);
			return response;
			}
		response.asCode(400);
		response.asMessage("Failed");
		response.asMessage("Failed To Save File");
		return response;
	}

	public boolean uploadFile(MultipartFile multipartFile) {
		boolean f = false;
		try {
			Files.copy(multipartFile.getInputStream(), Paths.get(path + File.separator + multipartFile.getOriginalFilename()));
			f = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return f;

	}
	
	@GetMapping("order/list")
	public RestResponse getOrderList() {
		RestResponse response = new RestResponse();
		List<OrderList> orders = orderListRepository.findAll();
		if (orders != null) {
		for(OrderList order: orders) {
			HashMap<String, Object> result= new HashMap<>();	
			result.put("id", order.getId());
			result.put("createdBy", order.getCreatedBy());
			result.put("createdDate", order.getCreatedDate());
			result.put("deliveryDate", order.getDeliveryDate());
			result.put("scanId", order.getScan().getId());
			result.put("status", order.getStatus());
			response.asCode(200);
			response.asMessage("Success");
			response.asData(result);
			
		}return response;
		} else {
			response.asCode(200);
			response.asMessage("Orders not found");
			return response;
		}
	}
}
