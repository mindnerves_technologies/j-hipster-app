package com.mnt.demo.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mnt.demo.service.PointCloudService;
import com.mnt.demo.web.rest.vm.PointCloudVM;
import com.mnt.demo.web.rest.vm.RestResponse;

@RestController
@RequestMapping("/api")
public class PointCloudResource {
	@Autowired
	private PointCloudService pointCloudService;

	@PostMapping("/points")
	public RestResponse savePoints(@RequestBody PointCloudVM pointCloudVM) {
		return pointCloudService.savePoints(pointCloudVM);
	}
	
	@PostMapping("/points/bigcommerce")
	public RestResponse savePoints(@RequestBody RestResponse restResponse) {
		return pointCloudService.saveBigCommerceResponse(restResponse);
	}
}
