package com.mnt.demo.web.rest.vm;

import java.util.List;

public class PointCloudVM {
	private String scanName;
	private List<?> points;

	public String getScanName() {
		return scanName;
	}

	public void setScanName(String scanName) {
		this.scanName = scanName;
	}

	public List<?> getPoints() {
		return points;
	}

	public void setPoints(List<?> points) {
		this.points = points;
	}
}
