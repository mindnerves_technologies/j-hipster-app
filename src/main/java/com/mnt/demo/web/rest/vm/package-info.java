/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mnt.demo.web.rest.vm;
