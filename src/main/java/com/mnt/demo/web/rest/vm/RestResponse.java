package com.mnt.demo.web.rest.vm;

import java.util.HashMap;

public class RestResponse extends HashMap<String, Object>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3233485143169824229L;
	

	public RestResponse asCode(Integer code) {
		put("code", code);
		return this;
	}

	public RestResponse asMessage(String message) {
		put("message", message);
		return this;
	}

	public  RestResponse asData(Object data) {
		put("data", data);
		return this;
	}

}
